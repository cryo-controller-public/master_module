#include "link.h"

static bool save_cfg(void);


/* --- Slave devices registers mapping --- */
//RHR
static uint16_t PKT_reg_vals[128];
static uint16_t heater_reg_vals[128];

//WSR
static uint16_t heater_ON_WSR         = 0;
static uint16_t heater_PWM_duty_cycle_WSR = 0; 

//Communication failure flags
static bool PKT_read_failure     = true;
static bool heater_read_failure  = true;
static bool heater_write_failure = true;
/* --------------------------------------- */


/*---- Modbus master debug ----*/
static uint16_t PKT_requests_count         = 0;
static uint16_t heater_requests_count      = 0;
static uint16_t heater_WSR_requests_count  = 0;

static uint16_t PKT_callbacks_count        = 0;
static uint16_t heater_callbacks_count     = 0;
static uint16_t heater_WSR_callbacks_count = 0;

static uint16_t PKT_err_count              = 0;
static uint16_t heater_err_count           = 0;
static uint16_t heater_WSR_err_count       = 0;
/*------------------------------*/


//static uint8_t dataI2C = 0;


/*-- Temperature and resistance recalculation --*/
#define T1           (T[0])
#define T1_modbus    (T_modbus[0])
#define R1_modbus    (R_modbus[0])
#define R1           (R[0])
#define TEMPERATURES_COUNT 8
static int32_t T_modbus[TEMPERATURES_COUNT] = {0};
static double  T[TEMPERATURES_COUNT]        = {0};
static int32_t R_modbus[TEMPERATURES_COUNT] = {0};
static double  R[TEMPERATURES_COUNT]        = {0};

static double R0_T1 = 1000.0;
static double T1_coeffs[7] = {   42.105278610659297600,
	                             -530.078432954847813000,
	                             2761.093500901013610000,
	                            -6970.407343246042730000,
	                             9125.941658139228820000,
	                            -5572.688362300395970000,
	                             1699.034148246049880000};


static void recalculate_T_R()
{
	R1_modbus  = PKT_reg_vals[0];
  R1_modbus |= PKT_reg_vals[1] << 16;
  R1 = (double)R1_modbus / 100.0;
	
	if (R1 == 0.0)
	{
		T1 = 0;
	} else {
		T1 = T1_coeffs[0] + 
		     T1_coeffs[1] * pow((R0_T1 / R1), 1) +
		     T1_coeffs[2] * pow((R0_T1 / R1), 2) +
		     T1_coeffs[3] * pow((R0_T1 / R1), 3) +
		     T1_coeffs[4] * pow((R0_T1 / R1), 4) +
		     T1_coeffs[5] * pow((R0_T1 / R1), 5) +
		     T1_coeffs[6] * pow((R0_T1 / R1), 6);
	}
	
	T1_modbus = (int32_t)(T1 * 100.0);
	
//	for (int i=1; i<TEMPERATURES_COUNT; i++)
//	{
//    R_modbus[i]  = PKT_reg_vals[2*i + 0];
//    R_modbus[i] |= PKT_reg_vals[2*i + 1] << 16;
//    R[i] = (double)R_modbus[i] / 100.0;
//    
//    T[i] = -(sqrt((-0.00232)*R[i] + 17.59246) - 3.908) / 0.00116;
//		T[i] += 273.15;
//    T_modbus[i] = (int32_t)(T[i] * 100.0);
//	}	
}
/*----------------------------------------------*/


/*----------- Regulator ----------*/
static double T1_sp               = 273.15;
static double regulator_max_power = 50.0;
static bool regulator_enabled = false;
PID_t PID;

static void regulator_init()
{
		PID_init(&PID);
		PID.kP = 50;
		PID.kI = 0.1;
		
		PID.Imax = 100;
		PID.Imin = 0;
}



static void regulator_tasks(int32_t dT_ms)
{
	  double dT = (double)dT_ms / 1000.0;
	
	  if(true == PKT_read_failure) regulator_enabled = false;
	  
	  if (false == regulator_enabled)
		{
			  heater_ON_WSR = 0;
			  heater_PWM_duty_cycle_WSR = 0;
			  PID_reset(&PID);
		} else {
			  double error = (T1_sp - T1);
			  double PID_out = PID_process(&PID, dT, error);
			  PID_out = clamp(PID_out, 0.0, regulator_max_power);
			  
			  heater_PWM_duty_cycle_WSR = (uint16_t)PID_out;
			  heater_ON_WSR = (heater_PWM_duty_cycle_WSR > 0) ? 1 : 0;
    }
}
/*------------------------------- */

//// Fast regulator template
///*----------- Regulator ----------*/
//static double T1_sp           = 273.15;
//static bool regulator_enabled = false;
//static double max_out = 100.0;
//PID_t PID;

//static void regulator_init()
//{
//		PID_init(&PID);
//		PID.kP = 50;
//		PID.kI = 0.1;
//		
//		PID.Imax = 100;
//		PID.Imin = 0;
//}

//static void regulator_tasks(int32_t dT_ms)
//{
//	  double dT = (double)dT_ms / 1000.0;
//	
//	  if(true == PKT_read_failure) regulator_enabled = false;
//	  
//	  if (false == regulator_enabled)
//		{
//			  heater_ON_WSR = 0;
//			  heater_PWM_duty_cycle_WSR = 0;
//			  PID_reset(&PID);
//		} else {
//			  double error = (T1_sp - T1);
//			  double fastreg_min_err = max_out / PID.kP;
//			  
//			  double PID_out = PID_process(&PID,
//					                            dT,
//				                             (error < fastreg_min_err) ? error : 0
//				                            );
//				
//				double reg_out = (error >= fastreg_min_err) ? max_out : PID_out;
//				reg_out = clamp(reg_out, 0, max_out);
//			  
//			  heater_PWM_duty_cycle_WSR = reg_out;
//			  heater_ON_WSR = (heater_PWM_duty_cycle_WSR > 0) ? 1 : 0;
//    }
//}
///*------------------------------- */


/*------ GUI data exchange ------*/
//int16_t GUI_touch_count = 0;
//void GUI_touch_count_inc(void)
//{
//	GUI_touch_count++;
//}

static double T1_sp_delta = 1;

double T1_sp_get(void)
{
	return T1_sp;
}

void T1_sp_inc(void)
{
	T1_sp += T1_sp_delta;
	save_cfg();
}

void T1_sp_dec(void)
{
	T1_sp -= T1_sp_delta;
	save_cfg();
}

void regulator_switch_state(void) // Enable / disable
{
	if (false == regulator_enabled)
		regulator_enabled = true;
	else
		regulator_enabled = false;
}

int regulator_get_state(void)
{
	return (regulator_enabled == true) ? 1 : 0;
}

static void Temperature_send_to_GUI()
{
	if (true == PKT_read_failure)   // Don't update temperatures until PKT connection stabilized
		return;
	
	extern char strBufNum[NUM_STR_LEN]; //TODO: REMAKE text displaying in GUI (see GUI_CTC_labelTSetNum func implementation)
	
	int32_t T1_int    = (int32_t)T1;
	int32_t T1_sp_int = (int32_t)T1_sp;
	
	GUI_CTC_labelTSetNum(LABEL_TCHANGE_NUM_ID, strBufNum, T1_int);
	GUI_CTC_labelTSetNum(LABEL_TSET_NUM_ID, strBufNum, T1_sp_int);
	
	
	static uint8_t T_GUI_IDs[] = {LABEL_SENS_1_NUM_ID,
                                LABEL_SENS_2_NUM_ID,
                                LABEL_SENS_3_NUM_ID,
                                LABEL_SENS_4_NUM_ID,
                                LABEL_SENS_5_NUM_ID,
                                LABEL_SENS_6_NUM_ID,
                                LABEL_SENS_7_NUM_ID,
                                LABEL_SENS_8_NUM_ID,
	};
	
	for (int i=0; i<TEMPERATURES_COUNT; i++)
	{
		GUI_CTC_labelTSetNum(T_GUI_IDs[i], strBufNum, (int32_t)T[i]);
	}
}
/*------------------------------- */


/*--------- Modbus slave ----------*/
static unsigned int RegisterAddr  = 0;
static unsigned int RegisterValue = 0;

static void modbus_poll(void)
{
    switch(modbus_get_poll())
    {
//////////////////////////// ������ HOLDING ////////////////////////// 
        case MODBUS_RHR_CMD:
            
            MODBUS_SLAVE_LED_ON;							// LED toggle
        
            // Measurements info
            holding_reg_write(0,  (uint32_t)T1_modbus & 0xffff); 
            holding_reg_write(1, ((uint32_t)T1_modbus >> 16) & 0xffff);
            
            holding_reg_write(2,  (uint32_t)R1_modbus & 0xffff); 
            holding_reg_write(3, ((uint32_t)R1_modbus >> 16) & 0xffff);
            
            
            // Regulator state
				    {
							  int16_t T1_sp_modbus = (int16_t)(T1_sp * 100.0);
							  holding_reg_write(5,  (uint16_t)T1_sp_modbus & 0xffff); 
						}
						holding_reg_write(6,  regulator_enabled ? 1 : 0);
						holding_reg_write(7,   (uint16_t)(uint32_t)   PID.kP);
						holding_reg_write(8,   (uint16_t)(uint32_t)   (PID.kI * 1000.0));
						holding_reg_write(9,   (uint16_t)(int16_t)    PID.Imax);
						holding_reg_write(10,  (uint16_t)(int16_t)    PID.Imin);
						holding_reg_write(11,  heater_PWM_duty_cycle_WSR);   //Heater target PWD duty cycle
						holding_reg_write(12,  (uint16_t)(int16_t)PID._P);
						holding_reg_write(13,  (uint16_t)(int16_t)PID._I);
						holding_reg_write(14,  (uint16_t)(int16_t)regulator_max_power);
            
            // Heater state
            holding_reg_write(20,  heater_reg_vals[0]);   //Heater EN/DIS
            holding_reg_write(21,  heater_reg_vals[1]);   //Heater voltage
            holding_reg_write(22,  heater_reg_vals[2]);   //Heater current
            holding_reg_write(23,  heater_reg_vals[3]);   //Heater power
            holding_reg_write(24,  heater_reg_vals[4]);   //Heater avg voltage
						holding_reg_write(25,  heater_reg_vals[5]);   //Heater PWD duty cycle
            
            
            // Slaves communication diagnostics
            holding_reg_write(30,  PKT_read_failure     ? 1 : 0);
            holding_reg_write(31,  heater_read_failure  ? 1 : 0);
            holding_reg_write(32,  heater_write_failure ? 1 : 0);
            

            //DEBUG INFO BEGIN
            for(int i=0; i<2; i++)
            {
            holding_reg_write(54+i,PKT_reg_vals[i]);
            }
            
            for(int i=0; i<4; i++)
            {
            holding_reg_write(57+i,heater_reg_vals[i]);
            }
            
            holding_reg_write(62,PKT_requests_count);
            holding_reg_write(63,PKT_callbacks_count);
            holding_reg_write(64,PKT_err_count);
            
            holding_reg_write(66,heater_requests_count);
            holding_reg_write(67,heater_callbacks_count);
            holding_reg_write(68,heater_err_count);
            
            holding_reg_write(70,heater_WSR_requests_count);
            holding_reg_write(71,heater_WSR_callbacks_count);
            holding_reg_write(72,heater_WSR_err_count);
						
//						extern int16_t GUI_touch_count;
//						holding_reg_write(74,(uint16_t)GUI_touch_count);
            //DEBUG INFO END
            
        
            modbus_rhr_answer(); 							// modbus rhr cmd answer
            MODBUS_SLAVE_LED_OFF; 	  				// LED toggle
        
        break;
//////////////////////////// ������ HOLDING ////////////////////////// 
        case MODBUS_WSR_CMD:  	 	 						// ������ holding ���������
        
            MODBUS_SLAVE_LED_ON;	 						// LED toggle
            modbus_wsr_answer();   						// ����� �� ������
        
            // ���������� ���������� ������������ ������� �� ������ ��������� 
        
            RegisterAddr  = get_wr_reg_addr();  // get address
            RegisterValue = get_wr_reg_val();   // get the new value
        
            {
							bool need_cfg_saving = true;
							
							switch(RegisterAddr)
							{ 
							case 0: 

							break;
							//=====
							case 5: 
							{
								int16_t T1_sp_modbus = (int16_t)RegisterValue;
								T1_sp = (double)T1_sp_modbus / 100.0;
							}
							break;
							//=====
							case 6: 
								regulator_enabled = (RegisterValue == 0) ? false : true;
								GUI_CTC_buttonStateUpdate();
							  need_cfg_saving = false;
							break;
							//=====
							case 7:						
								PID.kP = (double)RegisterValue;
							break;
							//=====
							case 8:				
								PID.kI = (double)RegisterValue / 1000.0;
							break;
							//=====
							case 9:
							{							
								int16_t Imax = (int16_t)RegisterValue;
								PID.Imax = (double)Imax;
							}
							break;
							//=====
							case 10:
							{							
								int16_t Imin = (int16_t)RegisterValue;
								PID.Imin = (double)Imin;
							}
							break;
							//=====
							case 14:
							{
								regulator_max_power = (double)(int16_t)RegisterValue;
								regulator_max_power = clamp(regulator_max_power, 0.0, 100.0);
							}
							break;
							//=====
							default:
								need_cfg_saving = false;
							break;
							}
							
							if (need_cfg_saving == true)
								save_cfg();
					  }
            MODBUS_SLAVE_LED_OFF;			// LED toggle

        break;
//////////////////////////// ������ INPUT ////////////////////////////
        case MODBUS_RIR_CMD:  // ������ input ���������
        
            MODBUS_SLAVE_LED_ON;					
            
        
            MODBUS_SLAVE_LED_OFF; // LED toggle
        
        modbus_rir_answer(); // ����� �� ������
        break;
        
    } // switch
}
/*------------------------------- */


/*--------- Modbus master --------*/
static MODBUS_MASTER mm_uart4;

//void mm_callback(MODBUS_RHR_RESPONSE response)
//{
//	static unsigned int inc = 0;
//	
//	if(false == response.err_flag)  //Error handling
//	{
//		inc++;
//	} else {
//		inc--;
//	}
//	
//	inc = inc % 100;
//	holding_reg_write(33, inc);
//}

static void mm_PKT_callback(MODBUS_RHR_RESPONSE response)
{
    PKT_callbacks_count++;
    
    if(true == response.err_flag)  //Error handling
    {
        PKT_err_count++;
        PKT_read_failure = true;
    } else {
        PKT_read_failure = false;
    }
}

static void mm_heater_callback(MODBUS_RHR_RESPONSE response)
{
    heater_callbacks_count++;
    
    if(true == response.err_flag)  //Error handling
    {
        heater_err_count++;
        heater_read_failure = true;
    } else {
        heater_read_failure = false;
    }
}

static void mm_heater_WSR_callback(MODBUS_WSR_RESPONSE response)
{
    heater_WSR_callbacks_count++;
    
    if(true == response.err_flag)  //Error handling
    {
        heater_WSR_err_count++;
        heater_write_failure = true;
    } else {
        heater_write_failure = false;
    }
}

static void poll_slave_devices()
{
    static int slave_selector = 0;
    
    if( false == modbus_master_busy(&mm_uart4) )
    {
        switch(slave_selector){
            // PKT reading registers
            case 0:
                PKT_requests_count++;
                modbus_master_rhr(&mm_uart4,
                                  100,
                                  1000,
                                  16,
                                  PKT_reg_vals,
                                  mm_PKT_callback,
                                  TIM4_Start,
                                  TIM4_Stop
                );
                
            // HEATER reading registers
            {slave_selector = __LINE__; break; case __LINE__: ;}
                heater_requests_count++;
                modbus_master_rhr(&mm_uart4,
                                  1,
                                  0,
                                  10,
                                  heater_reg_vals,
                                  mm_heater_callback,
                                  TIM4_Start,
                                  TIM4_Stop
                );
            
            // HEATER EN/DIS
            {slave_selector = __LINE__; break; case __LINE__: ;}
                heater_WSR_requests_count++;
                modbus_master_wsr(&mm_uart4,
                                  1,
                                  0,
                                  heater_ON_WSR,
                                  mm_heater_WSR_callback,
                                  TIM4_Start,
                                  TIM4_Stop
                );
						
            // HEATER PWM DUTY CYCLE
            {slave_selector = __LINE__; break; case __LINE__: ;}
                heater_WSR_requests_count++;
                modbus_master_wsr(&mm_uart4,
                                  1,
                                  5,
                                  heater_PWM_duty_cycle_WSR,
                                  mm_heater_WSR_callback,
                                  TIM4_Start,
                                  TIM4_Stop
                );
            
            default:
                slave_selector = 0;
                break;
        }
    }
}
/*------------------------------- */


 /*----- Config save/restore ------*/
 typedef struct
 {
	 double T1_sp;
	 
	 double PID_kP;
	 double PID_kI;
	 double PID_Imax;
	 double PID_Imin;

	 double regulator_max_power;
 } params2serialize_t;
 
 typedef union
 {
	 params2serialize_t p;
	 uint8_t b[sizeof(params2serialize_t)];
 } serializer_t;
 
 bool save_cfg(void)
 {
	 serializer_t serializer;
	 serializer.p.T1_sp    = T1_sp;
	 serializer.p.PID_kP   = PID.kP;
	 serializer.p.PID_kI   = PID.kI;
	 serializer.p.PID_Imax = PID.Imax;
	 serializer.p.PID_Imin = PID.Imin;
	 serializer.p.regulator_max_power = regulator_max_power;
	 
	 uint32_t tries_cnt = 5;
	 bool wr_success = write_cfg(serializer.b, sizeof(serializer.b), tries_cnt);
	 
	 return wr_success;
 }
 
 bool restore_cfg(void)
 {
	 serializer_t serializer;
	 
	 uint32_t tries_cnt = 5;
	 bool rd_success = read_cfg(serializer.b, sizeof(serializer.b), tries_cnt);
	 if (rd_success == false)
			 return false;
	 
	 T1_sp        = serializer.p.T1_sp;
	 PID.kP       = serializer.p.PID_kP;
	 PID.kI       = serializer.p.PID_kI;
	 PID.Imax     = serializer.p.PID_Imax;
	 PID.Imin     = serializer.p.PID_Imin;
	 regulator_max_power = serializer.p.regulator_max_power;
	 
	 return true;
 }
 /*------------------------------- */


//static void SPI_test_tasks(void)
//{
//	SPI1_CS_RESET;
//	SPIx_rxtx(SPI1, 0x01);
//	SPI1_CS_SET;
////	for(int i=0; i<8; i++)
////	{
////		GPIO_SetBits(GPIOA, GPIO_Pin_4);
////		delay_ms(5);
////		GPIO_ResetBits(GPIOA, GPIO_Pin_4);
////		delay_ms(5);
////	}
//}


/*----------- Main app -----------*/
int main(void)
{
    // ������������
    CPU_init();
    
    // GPIO
    GPIO_setup();
    
    // ������������� �������
    TIM6_DAC_init();
    TIM7_init();
    
    // ������������� SPI
	  SPIxInit(SPI1, 0, 1); //~13 MHz
    SPI3_init();
    
    // ������������� ������ I2C
    I2C1_init();
    
    // ������������� ������ UART2 - MODBUS DSE SLAVE
    UART2_init();
    set_modbus_id(1);
    modbus_init();	// slave device RTU over TCP 
    
    // ������������� ������ UART1 - MODBUS RS485 MASTER
    UART4_init();
    
    // interrupts
    //	NVIC_SetPriorityGrouping(4);
    //	NVIC_SetPriority(USART2_IRQn, 13);
    //	NVIC_SetPriority(UART4_IRQn,  12);
    //	NVIC_SetPriority(TIM2_IRQn, 	11);
    
    // ������������� LCD
    LCD_init();
    
    // ������������� touch ������
    FT6236_init();
    
    // ����� ��� LCD
    LCD_fill(0xBDD7);
    
    // init Graphical User Interface for CryoTargetController
    GUI_CTC_init();
    
    //modbus_master_init(&mm_uart4, MODBUS_UART4);
    modbus_master_init_uart4(&mm_uart4);
    init_timer_TIM4(&mm_uart4);
		
		regulator_init();
		
		// External interrupt init
    EXTI_init();
    
		//if (restore_cfg() == true)
		//	holding_reg_write(42, 1);
		restore_cfg();
		
    MODBUS_MASTER_LED_ON;	
    
    while(1)
    {
      modbus_master_tasks(&mm_uart4);
			
			// GUI cycle
			{
				static int32_t GUI_tick = 0;
			  const  int32_t GUI_period = 10;
				int32_t dT = (int32_t)(SysTickMs - GUI_tick);
				
				if(dT >= GUI_period)
				{
					GUI_tick = SysTickMs;
					GUI_Handler(dT); // touch display handler
				}
			}
			
			// Poll + regulator cycle
			{
				static int32_t poll_tick = 0;
				const int32_t poll_period = 100;
				int32_t dT = (int32_t)(SysTickMs - poll_tick);
				
				if ( dT >= poll_period )
				{
					poll_tick = SysTickMs;
					
					modbus_poll(); // modbus slave RTU over TCP
					poll_slave_devices();
					
					recalculate_T_R();
					Temperature_send_to_GUI();
					regulator_tasks(dT);
				}
			}
			
    }	
}
/*------------------------------- */
