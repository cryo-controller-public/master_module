#include "EEPROM.h"


void eeprom_read_buf (uint8_t *buf, uint32_t len)
{
	// EEPROM read sequence
  SPI1_CS_LOW;
  SPIx_rxtx(SPI1, EEPROM_READ);
  
	uint32_t addr = 0;
  SPIx_rxtx(SPI1, (uint8_t)(addr >> 8));      // set address ptr msb
  SPIx_rxtx(SPI1, (uint8_t)(addr & 0x00ff));  // set address ptr lsb

  // fill the array by the data from memory
  for(int i = 0; i < len; i++)
  {
    buf[i] = (uint8_t)SPIx_rxtx(SPI1, 0x00);
  }

  SPI1_CS_HIGH;
  // reading done
}

static void write_page(uint8_t *buf, uint32_t len, uint32_t addr)
{
  // EEPROM write enable sequence
  SPI1_CS_LOW;
  SPIx_rxtx(SPI1, EEPROM_WREN);
  SPI1_CS_HIGH;
  
  // EEPROM address and data write sequence
  SPI1_CS_LOW;
  SPIx_rxtx(SPI1, EEPROM_WRITE);
  
  SPIx_rxtx(SPI1, (unsigned char)(addr >> 8));      // set address ptr msb
  SPIx_rxtx(SPI1, (unsigned char)(addr & 0x00ff));  // set address ptr lsb
  
  for(int i = 0; i < len; i++)
    SPIx_rxtx(SPI1, buf[i]);     // save all data  

  SPI1_CS_HIGH;
  
  delay_ms(10);
}

void eeprom_write_buf(uint8_t *buf, uint32_t len)
{
	uint32_t page_len;
  for (uint32_t i=0; i<len; i+=64) //WRITE_REG 64 byte page
  {
    page_len = len - i;
    if( page_len > 64 ) page_len = 64;
    write_page(buf, page_len, i);
    buf += 64;
  }
}
