
#ifndef SPI_H
	#define SPI_H


	// подключение заголовочных файлов модулей проекта
	#include "link.h"


	// директивы работы с линией FSS
	#define SPI1_CS_LOW			GPIO_ResetBits(GPIOA, GPIO_Pin_4)
	#define SPI1_CS_HIGH		GPIO_SetBits(GPIOA, GPIO_Pin_4)
	
	#define SPI3_CS_RESET		GPIO_ResetBits(GPIOA, GPIO_Pin_15)
	#define SPI3_CS_SET			GPIO_SetBits(GPIOA, GPIO_Pin_15)


	// прототипы функций
	void SPI3_reset(void);								// деинициализация контроллера SSP
	void SPI3_init(void);									// инициализация нитерфейса SPI2
	void SPI3_sendData(uint16_t data);		// передача данных по интерфейсу SPI

  void SPIx_reset(SPI_TypeDef* SPIx);
  void SPIxInit(SPI_TypeDef* SPIx, uint8_t mode, uint8_t f);
  uint16_t SPIx_rxtx(SPI_TypeDef* SPIx, uint16_t data);

#endif
