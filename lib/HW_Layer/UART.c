
#include "UART.h"

/*=========================================================================== */
volatile unsigned char timer_state = 0;

unsigned volatile char uart4_rx_buf[128];    
unsigned volatile int uart4_rx_buf_index = 0;

extern volatile unsigned int rx_buf_ptr;
extern volatile unsigned char rx_buf[128];
extern volatile char rx_flag;
/*=========================================================================== */


// деинициализация модуля UART
void UARTx_reset(USART_TypeDef* UARTx)
{	
	USART_DeInit(UARTx);
}

// TIM2 init TODO - перенести в timer.c
void init_timer_TIM2(void) {

TIM_TimeBaseInitTypeDef base_timer;
TIM_TimeBaseStructInit(&base_timer);
	
base_timer.TIM_Prescaler = 26250-1;
base_timer.TIM_Period    = 40;
TIM_TimeBaseInit(TIM2, &base_timer);

TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);

NVIC_EnableIRQ(TIM2_IRQn);
}

// todo - общий инит через указатель

// инициализация нитерфейса UART1
void UART1_init(void)
{	
	UARTx_reset(USART1);
	
	USART1->BRR = 0x39;//72;  																			// baudrate 230400, 0xE4 for 115200
	USART1->CR1  |= USART_CR1_UE | USART_CR1_TE | USART_CR1_RE; 		// USART2 ON, TX ON, RX ON
	NVIC_SetPriority(USART1_IRQn, 0); 															
	
			
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE); 

	USART_Cmd(USART1, ENABLE);	
	NVIC_EnableIRQ(USART1_IRQn);  																
	
	//init_timer_TIM2();
}

// инициализация нитерфейса UART2
void UART2_init(void)
{	
	init_timer_TIM2();
	
	UARTx_reset(USART2);
	
	USART2->BRR = 0x72; //baudrate 230400
  //USART2->BRR = 0xE4; 			 // 0xE4 for 115200
	USART2->CR1  |= USART_CR1_UE | USART_CR1_TE | USART_CR1_RE; 		// USART2 ON, TX ON, RX ON
	NVIC_SetPriority(USART2_IRQn, 0); 															
	
			
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE); 

	USART_Cmd(USART2, ENABLE);	
	NVIC_EnableIRQ(USART2_IRQn);  																
}



// инициализация нитерфейса UART4
void UART4_init(void)
{	
	UARTx_reset(UART4);
	
	//UART4->BRR = 0x39;//72;// baudrate 230400
	UART4->BRR = 0xE4; 			 // 0xE4 for 115200
	UART4->CR1  |= USART_CR1_UE | USART_CR1_TE | USART_CR1_RE; 		// USART4 ON, TX ON, RX ON
	NVIC_SetPriority(UART4_IRQn, 0); 															
	
			
	USART_ITConfig(UART4, USART_IT_RXNE, ENABLE); 

	USART_Cmd(UART4, ENABLE);	
	NVIC_EnableIRQ(UART4_IRQn);  																
}


// передача одного байта через UART1 
// TODO - сделать 1 функцию для мноигх уарт
void Uart1SendByte(unsigned char TxByte)
{
		USART_SendData(USART1, (uint16_t)TxByte);
		while(USART_GetFlagStatus(USART1, USART_FLAG_TC)== RESET){}	
}

// передача одного байта через UART2
void Uart2SendByte(unsigned char TxByte)
{
	USART_SendData(USART2, (uint16_t)TxByte);
		while(USART_GetFlagStatus(USART2, USART_FLAG_TC)== RESET){}
}

// передача одного байта через UART4
void Uart4SendByte(unsigned char TxByte)
{
	USART_SendData(UART4, (uint16_t)TxByte);
		while(USART_GetFlagStatus(UART4, USART_FLAG_TC)== RESET){}
}



uint32_t y = 0;

// U1 1 byte rx int 
void USART1_IRQHandler (void){
	
  if (USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
    {  
			while(USART_GetFlagStatus(USART1,USART_FLAG_RXNE) == RESET){}
			y	 = USART1->DR;  // времянка
				
			USART_ClearITPendingBit(USART1, USART_IT_RXNE);

   }
}


// T2 int
void TIM2_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET) 
		TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
	
	TIM_Cmd(TIM2, DISABLE);
	timer_state = 0;
	rx_flag = 1;         // fire modbus cmd rx flag
}



// U2 1 byte rx int 
void USART2_IRQHandler (void){
	
			while(USART_GetFlagStatus(USART2,USART_FLAG_RXNE) == RESET){}
			
			if (rx_buf_ptr < 128)
			{
			  rx_buf[rx_buf_ptr++] = (unsigned char) USART2->DR;
			}	else {
				USART2->DR;
			}				
				
			USART_ClearITPendingBit(USART2, USART_IT_RXNE);
				
			if(timer_state == 0)
      {
        timer_state = 1;
				TIM2->CNT = 0;
			  TIM_Cmd(TIM2, ENABLE);
      }
}

//uint32_t temp_buf[32];
//uint8_t buf_index = 0;

// U4 1 byte rx int 
void UART4_IRQHandler (void){
	
  if (USART_GetITStatus(UART4, USART_IT_RXNE) != RESET)
    {  
			while(USART_GetFlagStatus(UART4,USART_FLAG_RXNE) == RESET){}
			
			if (uart4_rx_buf_index<128) {
				uart4_rx_buf[uart4_rx_buf_index++] = (unsigned char) UART4->DR;
			} else {
				UART4->DR;
			}

			USART_ClearITPendingBit(UART4, USART_IT_RXNE);

   }
}
