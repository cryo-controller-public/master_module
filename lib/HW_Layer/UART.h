
#ifndef UART_H
	#define UART_H


	// подключение заголовочных файлов модулей проекта
	#include "link.h"

  extern volatile unsigned char uart4_rx_buf[128];    
  extern volatile unsigned int uart4_rx_buf_index;

	// прототипы функций
	void UARTx_reset(USART_TypeDef* UARTx);
	void UART1_init(void);										// инициализация модуля UART1
	void UART2_init(void);									  // инициализация модуля UART2
	void UART4_init(void);									  // инициализация модуля UART3
	void Uart1SendByte(unsigned char TxByte);	// фунция отправки 1 байта через UART1 
	void Uart2SendByte(unsigned char TxByte);	// фунция отправки 1 байта через UART2 
	void Uart4SendByte(unsigned char TxByte);	// фунция отправки 1 байта через UART4 
	void USART1_IRQHandler(void);						  // U1 1 byte rx int 
	void USART2_IRQHandler(void);						  // U2 1 byte rx int 
	void UART4_IRQHandler(void);						  // U4 1 byte rx int 
	void TIM2_IRQHandler(void);								// T2 int handler

#endif
