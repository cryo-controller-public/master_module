#ifndef EEPROM_H
#define EEPROM_H

#include "link.h"

// 25LC128 SPI EEPROM
#define EEPROM_READ  0x03  // Read data from memory array beginning at selected address
#define EEPROM_WRITE 0x02  // Write data to memory array beginning at selected address  
#define EEPROM_WREN  0x06  // Set the write enable latch (enable write operations)
#define EEPROM_RDSR  0x05  // Read STATUS register

void eeprom_read_buf (uint8_t *buf, uint32_t len);
void eeprom_write_buf(uint8_t *buf, uint32_t len);

#endif //#ifndef EEPROM_H
