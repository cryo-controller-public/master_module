
#include "Timer.h"


volatile uint32_t Delay_dec = 0;
volatile int32_t  TIM6_tick = 0;


// TIM4 Modbus master uart4 timeout
static MODBUS_MASTER *tim4_mm = NULL;
// TIM4 init TODO - перенести в timer.c
void init_timer_TIM4(MODBUS_MASTER *mm) {
	tim4_mm = mm;
		
	TIM_TimeBaseInitTypeDef base_timer;
	TIM_TimeBaseStructInit(&base_timer);
	
	// ~4 seconds
	base_timer.TIM_Prescaler = 52500-1;
	base_timer.TIM_Period    = 4000;
	TIM_TimeBaseInit(TIM4, &base_timer);

	TIM_ITConfig(TIM4, TIM_IT_Update, ENABLE);

	NVIC_EnableIRQ(TIM4_IRQn);
}

// T4 int
void TIM4_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM4, TIM_IT_Update) != RESET) 
		TIM_ClearITPendingBit(TIM4, TIM_IT_Update);
	
	TIM_Cmd(TIM4, DISABLE);
	
	if(NULL != tim4_mm)
		modbus_master_abort(tim4_mm);
}

void TIM4_Start(void)
{
	TIM4->CNT = 0;
	TIM_Cmd(TIM4, ENABLE);
}

void TIM4_Stop(void)  //TODO: is counter reset need
{
	TIM_Cmd(TIM4, DISABLE);
}



void TIM6_DAC_init(void)
{
  TIM_TimeBaseInitTypeDef base_timer;
  TIM_TimeBaseStructInit(&base_timer);
  base_timer.TIM_Prescaler = 26250-1;
  base_timer.TIM_Period = 1;
  TIM_TimeBaseInit(TIM6, &base_timer);

  /* Разрешаем прерывание по обновлению (в данном случае -
   * по переполнению) счётчика таймера TIM6.
   */
  TIM_ITConfig(TIM6, TIM_IT_Update, ENABLE);
  /* Включаем таймер */
  TIM_Cmd(TIM6, ENABLE);

  /* Разрешаем обработку прерывания по переполнению счётчика
   * таймера TIM6. Так получилось, что это же прерывание
   * отвечает и за опустошение ЦАП.
   */
  NVIC_EnableIRQ(TIM6_DAC_IRQn);
}

void TIM6_DAC_IRQHandler(void)
{
  /* Так как этот обработчик вызывается и для ЦАП, нужно проверять,
   * произошло ли прерывание по переполнению счётчика таймера TIM6.
   */
  if(TIM_GetITStatus(TIM6, TIM_IT_Update) != RESET)
  {
		if(Delay_dec > 0)
			Delay_dec--;
		TIM6_tick++;
    /* Очищаем бит обрабатываемого прерывания */
    TIM_ClearITPendingBit(TIM6, TIM_IT_Update);
  }
}


void delay_ms(uint32_t Delay_ms_Data)
{
	Delay_dec = Delay_ms_Data;
	while(Delay_dec) {};
}


void TIM7_init(void)
{
  TIM_TimeBaseInitTypeDef base_timer;
  TIM_TimeBaseStructInit(&base_timer);
  base_timer.TIM_Prescaler = 26250-1;
  base_timer.TIM_Period = 1;
  TIM_TimeBaseInit(TIM7, &base_timer);

  TIM_ITConfig(TIM7, TIM_IT_Update, ENABLE);
  TIM_Cmd(TIM7, ENABLE);

  NVIC_EnableIRQ(TIM7_IRQn);
}


void TIM7_IRQHandler(void)
{
	for(uint8_t objButNum = 0; objButNum < OBJ_BUTTON_AMOUNT; objButNum++)
	{
		if(GUI.objList.ObjButtonList[objButNum].timerVal > 0)
		{
			GUI.objList.ObjButtonList[objButNum].timerVal--;
		}
		else
		{
			GUI.objList.ObjButtonList[objButNum].flag_buttonWasClicked = 0;
		}
	}
	
	TIM_ClearITPendingBit(TIM7, TIM_IT_Update);
}
