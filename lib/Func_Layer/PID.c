#include "PID.h"

void PID_reset(PID_t *PID)
{	
	PID->_P  = 0.0;
	PID->_I  = 0.0;
	PID->_D  = 0.0;
}

void PID_init(PID_t *PID)
{
	PID_reset(PID);
	
	PID->kP  = 0.0;
	PID->kI  = 0.0;
	PID->kD  = 0.0;
}

double PID_process(PID_t *PID, double dT, double error)
{
	PID->_P  = error * PID->kP;
	PID->_I += error * PID->kI * dT;
	PID->_D  = 0;
	
	PID->_I = clamp(PID->_I, PID->Imin, PID->Imax);
	
	double out = PID->_P + PID->_I + PID->_D;	
	return out;
}
