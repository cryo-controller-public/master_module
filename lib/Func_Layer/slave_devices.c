#include "slave_devices.h"
/*=========================================================================== */ 
 extern unsigned char uart2_rx_ptr; 
 extern unsigned char uart2_rx_buf[30];
 extern unsigned char answer_ready; 
 
 extern unsigned char bufx[30];
 
 //TODO: �� ������ ��� ���������� ����������?
 unsigned char timer3_timeout;
 unsigned char timer5_timeout;
 unsigned char timer7_timeout;
 
//void PSD4_struct_init(PSD4cmds cmd)
// {
//     cmd.init_protocol   = 10;
//     cmd.init_valve      = 11;
//     cmd.init_syringe    = 12;
//     
//     cmd.get_syr_pos     = 13;
//     cmd.get_syr_vel     = 14;
//     cmd.get_valve_angle = 15;
//
//     cmd.set_syr_pos     = 16;
//     cmd.set_syr_vel     = 17;
//     cmd.set_valve_angle = 18;
// }

 // start/stop async delay timer
void TmOutTimer3_state(unsigned int state)
{
    T2CONbits.TON = state;  // Start 32-bit Timer
}

 // T2/3 - async delay timer set
 void TmOutTimer3_set(unsigned int ms)
 {
    unsigned long msCnts = (unsigned long)ms * 1000UL;  // user ms into us
    msCnts = (unsigned long)((float)msCnts / 4.26);      // us to timer ticks
    
    TMR3 = 0x00;            // Clear 32-bit Timer (msw)
    TMR2 = 0x00;            // Clear 32-bit Timer (lsw)
    
    PR3 = (unsigned int)(msCnts >> 16);        // Load 32-bit period value (msw)
    PR2 = (unsigned int)(msCnts & 0x0000ffff);  // Load 32-bit period value (lsw)
 }
 
// T2/3 - async delay timer
void TmOutTimer3_init(void)
{
     // 1 timer tick = 60M/256 = 234 375 Hz = 4.26 us
     // 176 056 (2 AFB8) = 750 ms
      
    T3CONbits.TON = 0;      // Stop any 16-bit Timer3 operation
    T2CONbits.TON = 0;      // Stop any 16/32-bit Timer3 operation
    T2CONbits.T32 = 1;      // Enable 32-bit Timer mode
    T2CONbits.TCS = 0;      // Select internal instruction cycle clock
    T2CONbits.TGATE = 0;    // Disable Gated Timer mode
    T2CONbits.TCKPS = 0b11; // Select 1:256 Prescaler

    TMR3 = 0x00;            // Clear 32-bit Timer (msw)
    TMR2 = 0x00;            // Clear 32-bit Timer (lsw)
    
    // defaul - any values
    PR3 = 0x0000;      // Load 32-bit period value (msw)
    PR2 = 0x00ff;      // Load 32-bit period value (lsw)
    
    IPC2bits.T3IP = 4;      // Set Timer3 Interrupt Priority Level
    IFS0bits.T3IF = 0;      // Clear Timer3 Interrupt Flag
    
    IEC0bits.T3IE = 1;      // Enable Timer3 interrupt
}

// T2/3 async delay timer int
void _ISR_PSV _T3Interrupt(void)
{   
    //PSD4rxToutOvfl = 1;
    timer3_timeout = 1; 

    TMR2 = 0x00; // Clear 32-bit Timer (msw)
    TMR3 = 0x00; // Clear 32-bit Timer (lsw)

    IFS0bits.T3IF = 0;   // Clear Timer interrupt flag  
} 

 
 
 
 
// start/stop async delay timer
void TmOutTimer5_state(unsigned int state)
{
    T4CONbits.TON = state;  // Start 32-bit Timer
}

 // T4/5 - async delay timer set
 void TmOutTimer5_set(unsigned int ms)
 {
    unsigned long msCnts = (unsigned long)ms * 1000UL;  // user ms into us
    msCnts = (unsigned long)((float)msCnts / 4.26);      // us to timer ticks
    
    TMR5 = 0x00;            // Clear 32-bit Timer (msw)
    TMR4 = 0x00;            // Clear 32-bit Timer (lsw)
    
    PR5 = (unsigned int)(msCnts >> 16);        // Load 32-bit period value (msw)
    PR4 = (unsigned int)(msCnts & 0x0000ffff);  // Load 32-bit period value (lsw)
 }
 
// T4/5 - async delay timer
void TmOutTimer5_init(void)
{
     // 1 timer tick = 60M/256 = 234 375 Hz = 4.26 us
     // 176 056 (2 AFB8) = 750 ms
      
    T5CONbits.TON = 0;      // Stop any 16-bit Timer5 operation
    T4CONbits.TON = 0;      // Stop any 16/32-bit Timer5 operation
    T4CONbits.T32 = 1;      // Enable 32-bit Timer mode
    T4CONbits.TCS = 0;      // Select internal instruction cycle clock
    T4CONbits.TGATE = 0;    // Disable Gated Timer mode
    T4CONbits.TCKPS = 0b11; // Select 1:256 Prescaler

    TMR5 = 0x00;            // Clear 32-bit Timer (msw)
    TMR4 = 0x00;            // Clear 32-bit Timer (lsw)
    
    // defaul - any values
    PR5 = 0x0000;      // Load 32-bit period value (msw)
    PR4 = 0x00ff;      // Load 32-bit period value (lsw)
    
    IPC7bits.T5IP = 4;      // Set Timer5 Interrupt Priority Level
    IFS1bits.T5IF = 0;      // Clear Timer5 Interrupt Flag
    
    IEC1bits.T5IE = 1;      // Enable Timer5 interrupt
}

// T4/5 async delay timer int
void _ISR_PSV _T5Interrupt(void)
{   
    //PSD4rxToutOvfl = 1;
    timer5_timeout = 1; 

    TMR4 = 0x00; // Clear 32-bit Timer (msw)
    TMR5 = 0x00; // Clear 32-bit Timer (lsw)

    IFS1bits.T5IF = 0;   // Clear Timer interrupt flag  
} 


// start/stop async delay timer
void TmOutTimer7_state(unsigned int state)
{
    T6CONbits.TON = state;  // Start 32-bit Timer
}

 // T6/7 - async delay timer set
 void TmOutTimer7_set(unsigned int ms)
 {
    unsigned long msCnts = (unsigned long)ms * 1000UL;  // user ms into us
    msCnts = (unsigned long)((float)msCnts / 4.26);      // us to timer ticks
    
    TMR7 = 0x00;            // Clear 32-bit Timer (msw)
    TMR6 = 0x00;            // Clear 32-bit Timer (lsw)
    
    PR7 = (unsigned int)(msCnts >> 16);        // Load 32-bit period value (msw)
    PR6 = (unsigned int)(msCnts & 0x0000ffff);  // Load 32-bit period value (lsw)
 }
 
// T6/7 - async delay timer
void TmOutTimer7_init(void)
{
     // 1 timer tick = 60M/256 = 234 375 Hz = 4.26 us
     // 176 056 (2 AFB8) = 750 ms
      
    T7CONbits.TON = 0;      // Stop any 16-bit Timer5 operation
    T6CONbits.TON = 0;      // Stop any 16/32-bit Timer5 operation
    T6CONbits.T32 = 1;      // Enable 32-bit Timer mode
    T6CONbits.TCS = 0;      // Select internal instruction cycle clock
    T6CONbits.TGATE = 0;    // Disable Gated Timer mode
    T6CONbits.TCKPS = 0b11; // Select 1:256 Prescaler

    TMR7 = 0x00;            // Clear 32-bit Timer (msw)
    TMR6 = 0x00;            // Clear 32-bit Timer (lsw)
    
    // defaul - any values
    PR7 = 0x0000;      // Load 32-bit period value (msw)
    PR6 = 0x00ff;      // Load 32-bit period value (lsw)
    
    IPC12bits.T7IP = 4;      // Set Timer7 Interrupt Priority Level
    IFS3bits.T7IF = 0;      // Clear Timer7 Interrupt Flag
    
    IEC3bits.T7IE = 1;      // Enable Timer7 interrupt
}

// T6/7 async delay timer int
void _ISR_PSV _T7Interrupt(void)
{   
    //PSD4rxToutOvfl = 1;
    timer7_timeout = 1; 

    TMR6 = 0x00; // Clear 32-bit Timer (msw)
    TMR7 = 0x00; // Clear 32-bit Timer (lsw)

    IFS3bits.T7IF = 0;   // Clear Timer interrupt flag  
} 
  
unsigned char char2num (unsigned char ch)
{ // converts char symbol into number
  return ch - 0x30;
}

unsigned char num2char (unsigned char ch)
{ // converts number into char symbol
  return ch + 0x30;
}

unsigned int chars2number(unsigned char data_size, unsigned char begin_index)
{
      unsigned int converted_num = 0;
      
      switch(data_size) // 1-9, 10-99, 100 -- 999, 1000+ cases
      {
          case 1:  
              converted_num = char2num(uart2_rx_buf[begin_index]);
          break;
          //----
          case 2:  
              converted_num = char2num(uart2_rx_buf[begin_index])  * 10 +
                         char2num(uart2_rx_buf[begin_index+1]);
          break;
          //----
          case 3:  
              converted_num = char2num(uart2_rx_buf[begin_index])  * 100 +
                         char2num(uart2_rx_buf[begin_index+1])* 10  +
                         char2num(uart2_rx_buf[begin_index+2]);
          break;
          //----
          case 4:  
              converted_num = char2num(uart2_rx_buf[begin_index])  * 1000 +
                         char2num(uart2_rx_buf[begin_index+1])* 100  +
                         char2num(uart2_rx_buf[begin_index+2])* 10   +
                         char2num(uart2_rx_buf[begin_index+3]);
          //----
          default: break;
        }
}

void answer_refresh(void)
{
    // clear RxArray for the new cmd and answer
    
    uart2_rx_ptr = 0;
    for(char i = 0; i < 30; i++)
        uart2_rx_buf[i] = 0;
     //   input_register[10+i] = 0; // 10+ - send + return bytes
}

