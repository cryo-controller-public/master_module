#include "link.h"

static unsigned int modbus_CRC16(unsigned char buf[], unsigned int len)
{
    unsigned int crc = 0xFFFF;
    //U8 crc_lsb, crc_msb;
    for (unsigned int pos = 0; pos < len; pos++)
    {
        crc ^= (unsigned int)buf[pos];          // XOR byte into least sig. byte of crc
        for (int i = 8; i != 0; i--)
        {    // Loop over each bit
            if ((crc & 0x0001) != 0)
            {      // If the LSB is set
                crc >>= 1;                // Shift right and XOR 0xA001
                crc ^= 0xA001;
            }
            else                          // Else LSB is not set
                crc >>= 1;                  // Just shift right
        }
    }
    
    // Note, this number has low and high bytes swapped, 
    // so use it accordingly (or swap bytes)
    // swapping bytes
    crc = ((crc<<8)&0xff00)|((crc>>8)&0x00ff);
    
    return crc;
}

bool modbus_master_busy(MODBUS_MASTER *mm)
{
	  if(MM_IDLE == mm->state)
		{
			  return false;
		} else {
			  return true;
		}
}

//bool modbus_master_init(MODBUS_MASTER *mm, MODBUS_BACKEND_ID backend_id)
//{
//    switch(backend_id)
//    {
//        case MODBUS_UART4:
//            mm->backend_id = MODBUS_UART4;
//            mm->send_byte = Uart4SendByte;
//            mm->clear_buf = NULL;
//            mm->rx_buf = uart4_rx_buf;
//            mm->rx_buf_index = &uart4_rx_buf_index;
//            break;
//            
//        default:
//            return false;
//    }
//    
//    mm->state = MM_IDLE;
//    
////    mm->rhr_callback = NULL;
////		mm->rhr_timer_start = NULL;
////		mm->rhr_timer_stop = NULL;
////		
////    mm->rhr_err_flag = false;
////    mm->rhr_dev_addr = 0;
////    mm->rhr_regs_count = 0;
////    mm->rhr_reg_vals = NULL;
//    
//    return true;            
//}

bool modbus_master_init_uart4(MODBUS_MASTER *mm)
{
		mm->backend_id = MODBUS_UART4;
		mm->send_byte = Uart4SendByte;
		//mm->clear_buf = NULL;
		mm->rx_buf = (unsigned char *)uart4_rx_buf;
		mm->rx_buf_index = (unsigned int *)&uart4_rx_buf_index;
    
    mm->state = MM_IDLE;
    
    return true;            
}

bool modbus_master_rhr(MODBUS_MASTER *mm,
                       uint8_t dev_addr,
                       uint16_t reg_addr,
                       uint16_t regs_count, 
                       uint16_t *reg_vals,
                       void (*response_callback)(MODBUS_RHR_RESPONSE),
											 void (*start_timer_func)(void),
											 void (*stop_timer_func)(void)
)
{
    if (mm->state != MM_IDLE)
        return false;
    
    mm->state = MM_RHR;
    
    mm->dev_addr = dev_addr;
    mm->reg_addr = reg_addr;
    mm->rhr_regs_count = regs_count;
    mm->rhr_reg_vals = reg_vals;
    
		mm->rhr_callback = response_callback;
        mm->wsr_callback = NULL;
		mm->timer_start = start_timer_func;
		mm->timer_stop = stop_timer_func;

    uint8_t send_buffer[8];
    send_buffer[0] = dev_addr;
    send_buffer[1] = 0x03; // RHR command ID
    
    //register addr
    send_buffer[2] = (reg_addr >> 8)&0x00FF; // first reg address byte
    send_buffer[3] = (reg_addr)&0x00FF;      // last reg address byte
    
    //count of registers to read
    send_buffer[4] = (regs_count >> 8)&0x00FF;
    send_buffer[5] = (regs_count)&0x00FF;
    
    //CRC
    unsigned int crc;
    crc = modbus_CRC16(send_buffer, 6);
    send_buffer[6] = (crc >> 8)&0x00FF;
    send_buffer[7] = (crc)&0x00FF;
        
    if(MODBUS_UART4 == mm->backend_id)
			MODBUS_MASTER_TX_EN;
		
    switch(mm->backend_id)
    {
//         case MODBUS_UART2:
//             // Uart2 specific actions to send command
//             break;
            
        default:
            if (NULL != mm->send_byte)
            {
                int i;
                for (i=0; i<8; i++)
                {
                    mm->send_byte(send_buffer[i]);
                }
            }
                
            break;
    }
		
		if(MODBUS_UART4 == mm->backend_id)
			MODBUS_MASTER_TX_DIS;
    
		if(NULL != mm->timer_start)
			mm->timer_start();
    
    
    return true;
}

bool modbus_master_wsr(MODBUS_MASTER *mm,
                       uint8_t dev_addr,
                       uint16_t reg_addr,
                       uint16_t reg_val,
                       void (*response_callback)(MODBUS_WSR_RESPONSE),
                       void (*start_timer_func)(void),
                       void (*stop_timer_func)(void)
)
{
    if (mm->state != MM_IDLE)
        return false;
    
    mm->state = MM_WSR;
    
    mm->dev_addr = dev_addr;
    mm->reg_addr = reg_addr;
    mm->wsr_reg_val = reg_val;
    
    mm->rhr_callback = NULL;
		mm->wsr_callback = response_callback;
		mm->timer_start = start_timer_func;
		mm->timer_stop = stop_timer_func;

    uint8_t send_buffer[8];
    send_buffer[0] = dev_addr;
    send_buffer[1] = 0x06; // WSR command ID
    
    //register addr
    send_buffer[2] = (reg_addr >> 8)&0x00FF; // first reg address byte
    send_buffer[3] = (reg_addr)&0x00FF;      // last reg address byte
    
    //value to write
    send_buffer[4] = (reg_val >> 8)&0x00FF;
    send_buffer[5] = (reg_val)&0x00FF;
    
    //CRC
    unsigned int crc;
    crc = modbus_CRC16(send_buffer, 6);
    send_buffer[6] = (crc >> 8)&0x00FF;
    send_buffer[7] = (crc)&0x00FF;
        
    if(MODBUS_UART4 == mm->backend_id)
			MODBUS_MASTER_TX_EN;
		
    switch(mm->backend_id)
    {
//         case MODBUS_UART2:
//             // Uart2 specific actions to send command
//             break;
            
        default:
            if (NULL != mm->send_byte)
            {
                int i;
                for (i=0; i<8; i++)
                {
                    mm->send_byte(send_buffer[i]);
                }
            }
                
            break;
    }
		
		if(MODBUS_UART4 == mm->backend_id)
			MODBUS_MASTER_TX_DIS;
    
		if(NULL != mm->timer_start)
			mm->timer_start();
		
    
    return true;
}

static void _callback(MODBUS_MASTER *mm)
{
  if (NULL != mm->rhr_callback)
	{
			MODBUS_RHR_RESPONSE rhr_response = {
				.err_flag = mm->err_flag,          //false - no error, true - error
				.dev_addr = mm->dev_addr,
				.reg_addr = mm->reg_addr,
				.regs_count = mm->rhr_regs_count,
				.reg_vals = mm->rhr_reg_vals
			};
			mm->rhr_callback(rhr_response);
	}
	
	if (NULL != mm->wsr_callback)
	{
			MODBUS_WSR_RESPONSE wsr_response = {
				.err_flag = mm->err_flag,          //false - no error, true - error
				.dev_addr = mm->dev_addr,
				.reg_addr = mm->reg_addr,
				.reg_val = mm->wsr_reg_val
			};
			mm->wsr_callback(wsr_response);
	}
}

void modbus_master_abort(MODBUS_MASTER *mm)
{
	if (mm->state != MM_IDLE)
		mm->state = MM_ABORT;
}

void modbus_master_tasks(MODBUS_MASTER *mm)
{
    unsigned int full_size;
    
    switch(mm->state)
    {
        case MM_IDLE:
            break;
            
        case MM_RHR:   //TODO: ������� ������ ��������� ������ - ��������?
            full_size = 5 + 2*(mm->rhr_regs_count);
            
            if (*(mm->rx_buf_index) >= full_size)      // response is full
            {
							  if(NULL != mm->timer_stop)
										mm->timer_stop();
							  
                unsigned int crc, crc_from_response;
                crc = modbus_CRC16(mm->rx_buf, full_size-2);
                crc_from_response = (mm->rx_buf[full_size-2] << 8)
                | (mm->rx_buf[full_size-1]);
                
                if (mm->rx_buf[0] != mm->dev_addr) // wrong device address
                {
                    mm->err_flag = true;
                }
                if (mm->rx_buf[1] != 0x03) // wrong command
                {
                    mm->err_flag = true;
                }
                else if (crc == crc_from_response)              // CRC is OK
                {
                    for (int i = 0; i<(mm->rhr_regs_count); i++)
                    {
                        mm->rhr_reg_vals[i] = (mm->rx_buf[3 + i*2] & 0xFF) << 8;
                        mm->rhr_reg_vals[i] |= mm->rx_buf[4 + i*2] & 0xFF;
                    }
                    
                    mm->err_flag = false;
                } else {                                        // CRC error
                    mm->err_flag = true;
                }
                
                *(mm->rx_buf_index) = 0;
                mm->state = MM_IDLE;
                
                _callback(mm);
                
            } else {                   // response is not full, waiting
                
            }
            
            break;
            
        case MM_WSR:
            //full_size = 8;
            if (*(mm->rx_buf_index) >= 8)      // response is full
            {
                if(NULL != mm->timer_stop)
                    mm->timer_stop();
                
                unsigned int crc, crc_from_response;
                crc = modbus_CRC16(mm->rx_buf, 6);
                crc_from_response = (mm->rx_buf[6] << 8) | (mm->rx_buf[7]);
                
                if (mm->rx_buf[0] != mm->dev_addr) // wrong device address
                {
                    mm->err_flag = true;
                }
                if (mm->rx_buf[1] != 0x06) // wrong command
                {
                    mm->err_flag = true;
                } else if (crc == crc_from_response)  // CRC is OK
                {
                    mm->err_flag = false;
                } else {                       // CRC error
                    mm->err_flag = true;
                }
                
                *(mm->rx_buf_index) = 0;
                mm->state = MM_IDLE;
                
                _callback(mm);
                
            } else {               // response is not full, waiting
                
            }
            
            break;
						
				case MM_ABORT:
					  *(mm->rx_buf_index) = 0;
						mm->state = MM_IDLE;
						
						if(NULL != mm->timer_stop)
							mm->timer_stop();
						
						mm->err_flag = true;  //false - no error, true - error
						_callback(mm);
						
						break;
				default:
					  mm->state = MM_IDLE;
    }
}
