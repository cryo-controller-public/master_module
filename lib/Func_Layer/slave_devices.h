
#include "xc.h"
#include "dialtek_uart.h"

#ifndef SLAVE_DIVICES_H
#define	SLAVE_DIVICES_H
/*=========================================================================== */ 
#define HEATER_TX_EN  LATFbits.LATF13 = 1  
#define HEATER_TX_DIS LATFbits.LATF13 = 0 
#define OVEN_TX_EN    LATBbits.LATB15 = 1 
#define OVEN_TX_DIS   LATBbits.LATB15 = 0

#define ERROR_CODE 65535  // value on reading error
#define default_id (unsigned char)1


 // start/stop async delay timer
void TmOutTimer3_state(unsigned int state);

 // T2/3 - async delay timer set
 void TmOutTimer3_set(unsigned int ms);
 
// T2/3 - async delay timer
void TmOutTimer3_init(void);

// T2/3 async delay timer int
void _ISR_PSV _T3Interrupt(void);

// start/stop async delay timer
void TmOutTimer5_state(unsigned int state);

// T4/5 - async delay timer set
void TmOutTimer5_set(unsigned int ms);
 
// T4/5 - async delay timer
void TmOutTimer5_init(void);

void _ISR_PSV _T5Interrupt(void);

// start/stop async delay timer
void TmOutTimer7_state(unsigned int state);

// T4/5 - async delay timer set
void TmOutTimer7_set(unsigned int ms);
 
// T4/5 - async delay timer
void TmOutTimer7_init(void);

void _ISR_PSV _T7Interrupt(void);

unsigned char char2num (unsigned char ch); // convert char to number

unsigned char num2char (unsigned char ch); // convert number to char

unsigned int check_cmd_answer(unsigned char index_1); // check the answer to a cmd 

void answer_refresh(void);		   // clear bufs and rx ptr

/*=========================================================================== */ 
#endif	

