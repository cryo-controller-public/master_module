#ifndef CONFIG_RW_H
#define CONFIG_RW_H

#include "link.h"

bool read_cfg(uint8_t *buf, uint32_t len, uint32_t tries_cnt);
bool write_cfg(uint8_t *buf, uint32_t len, uint32_t tries_cnt);

#endif //#ifndef CONFIG_RW_H
