/* 
 * File:   modbus_master.h
 * Author: ADM
 *
 * Created on 29 ���� 2020 �., 14:24
 */

#ifndef MODBUS_MASTER_H
#define	MODBUS_MASTER_H

#include "link.h"

#ifdef	__cplusplus
extern "C"
{
#endif
	
    
#define MAX_RHR_REGISTERS 120    // Max registers count in RHR request/response

typedef enum
{
    MODBUS_UART1,
    MODBUS_UART2,
    MODBUS_UART4,
    MODBUS_SPI1,
} MODBUS_BACKEND_ID;

typedef enum
{
    MM_IDLE,
    MM_RHR,
    MM_WSR,
	  MM_ABORT
} MODBUS_MASTER_STATE;

typedef struct
{
    bool err_flag;          //false - no error, true - error
    uint8_t dev_addr;
    uint16_t reg_addr;
    uint16_t regs_count;
    uint16_t *reg_vals;
} MODBUS_RHR_RESPONSE;

typedef struct
{
    bool err_flag;          //false - no error, true - error
    uint8_t dev_addr;
    uint16_t reg_addr;
    uint16_t reg_val;
} MODBUS_WSR_RESPONSE;

typedef struct
{
	  //All fields are private. User should not change them from the outside
    MODBUS_BACKEND_ID backend_id;
    void (*send_byte)(unsigned char);
    //void (*clear_buf)(void);
    unsigned char *rx_buf;
    unsigned int *rx_buf_index;
    
    MODBUS_MASTER_STATE state;
    
    void (*timer_start)(void);    //Common response vars
    void (*timer_stop)(void);
    bool err_flag;
    uint8_t dev_addr;
    uint16_t reg_addr;
    
    void (*rhr_callback)(MODBUS_RHR_RESPONSE);  //RHR specific vars
    uint16_t rhr_regs_count;
    uint16_t *rhr_reg_vals;
    
    void (*wsr_callback)(MODBUS_WSR_RESPONSE);  //WSR specific vars
    uint16_t wsr_reg_val;
    
} MODBUS_MASTER;

//bool modbus_master_init(MODBUS_MASTER *mm, MODBUS_BACKEND_ID backend_id);
bool modbus_master_init_uart4(MODBUS_MASTER *mm);
void modbus_master_tasks(MODBUS_MASTER *mm);
bool modbus_master_rhr(MODBUS_MASTER *mm,
                       uint8_t dev_addr,
                       uint16_t reg_addr,
                       uint16_t regs_count, 
                       uint16_t *reg_vals,  // buffer to write registers values
                       void (*response_callback)(MODBUS_RHR_RESPONSE),
											 void (*start_timer_func)(void),
											 void (*stop_timer_func)(void)
												 );

bool modbus_master_wsr(MODBUS_MASTER *mm,
                       uint8_t dev_addr,
                       uint16_t reg_addr,
                       uint16_t reg_val,
                       void (*response_callback)(MODBUS_WSR_RESPONSE),
                       void (*start_timer_func)(void),
                       void (*stop_timer_func)(void)
                       );

void modbus_master_abort(MODBUS_MASTER *mm);
bool modbus_master_busy(MODBUS_MASTER *mm);

#ifdef	__cplusplus
}
#endif

#endif	/* MODBUS_MASTER_H */

