#ifndef PID_h
#define PID_h

#include "link.h"

#define clamp(val, min, max) (val > max) ? max : (val < min) ? min : val

typedef struct
{
	double kP;
	double kI;
	double kD;
	
	double Imax;
	double Imin;
	
	double _P;
	double _I;
	double _D;
} PID_t;

void PID_reset(PID_t *PID);
void PID_init(PID_t *PID);
double PID_process(PID_t *PID, double dT, double error);

#endif //#ifndef PID_h
