
#ifndef LINK_H
	#define LINK_H


	// подключение стандартных библиотек
	#include "stm32f4xx.h"                  // Device header
	
	#include "stm32f4xx_syscfg.h"
	#include "stm32f4xx_gpio.h"
	#include "stm32f4xx_i2c.h"
	#include "stm32f4xx_rcc.h"
	#include "stm32f4xx_spi.h"
	#include "stm32f4xx_tim.h"
	#include "stm32f4xx_exti.h"
	#include "stm32f4xx_usart.h"	
	#include "misc.h"
	
	#include "stdint.h"
	#include "stddef.h"
	#include "stdbool.h"
	#include <math.h>
	#include <string.h>

	// подключение заголовочных файлов модулей проектов
	#include "modbus_master.h"
	#include "CPU.h"
	#include "GPIO.h"
	#include "EXTI.h"
	#include "Timer.h"
	#include "SPI.h"
	#include "I2C.h"
	#include "UART.h"
	#include "Font.h"
	#include "LCD_ILI9341.h"
	#include "Touch_FT6236.h"
	#include "GUI.h"
	#include "GUI_cryoTargetController.h"
	#include "modbus.h"
	#include "PID.h"
	#include "config_RW.h"
	#include "EEPROM.h"
	
	


#endif
