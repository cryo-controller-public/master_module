#include "config_RW.h"

/* расчет контрольной суммы */
static unsigned int modbus_CRC16(unsigned char buf[], unsigned int len)
{
    /// расчет crc16
    
    unsigned int crc = 0xFFFF;
    //U8 crc_lsb, crc_msb;
    for (unsigned int pos = 0; pos < len; pos++)
    {
        crc ^= (unsigned int)buf[pos];          // XOR byte into least sig. byte of crc
        for (int i = 8; i != 0; i--)
        {    // Loop over each bit
            if ((crc & 0x0001) != 0)
            {      // If the LSB is set
                crc >>= 1;                // Shift right and XOR 0xA001
                crc ^= 0xA001;
            }
            else                          // Else LSB is not set
                crc >>= 1;                  // Just shift right
        }
    }
    
    // Note, this number has low and high bytes swapped, 
    // so use it accordingly (or swap bytes)
    // swapping bytes
    crc = ((crc<<8)&0xff00)|((crc>>8)&0x00ff);
    
    return crc;
}

bool read_cfg(uint8_t *buf, uint32_t len, uint32_t tries_cnt)
{
    uint8_t rd_buf[len+2];        //data+CRC
    
    for(uint32_t i=0; i<tries_cnt; i++)
    {
        eeprom_read_buf(rd_buf, len+2);
        uint16_t crc = ((uint16_t)rd_buf[len] << 8) | (uint16_t)rd_buf[len+1];
        uint16_t crc_calc = modbus_CRC16(rd_buf, len);
        
        if(crc == crc_calc)
        {
            for(uint32_t j=0; j<len; j++)
                buf[j] = rd_buf[j];
            return true;
        }
    }
    
    return false; //Чтение не удалось после tries_cnt попыток
}

bool write_cfg(uint8_t *buf, uint32_t len, uint32_t tries_cnt)
{
    uint8_t wr_buf[len+2];
    uint16_t crc_calc = modbus_CRC16(buf, len);
    
    for(uint32_t j=0; j<len; j++)
        wr_buf[j] = buf[j];
    wr_buf[len]   = (crc_calc >> 8);
    wr_buf[len+1] =  crc_calc & 0x00ff;
    
    for(uint32_t i=0; i<tries_cnt; i++)
    {
        // Запись буфера с CRC в память
        eeprom_write_buf(wr_buf, len+2);
        
        // Повторное чтение буфера
        uint8_t rd_buf[len+2];
			  for(int i=0; i<(len+2); i++)
			  {
					rd_buf[i] = 0;
				}
        eeprom_read_buf(rd_buf, len+2);
        
        // Сравнение записанного буфера с повторно прочитанным на наличие ошибок записи
        if(memcmp(wr_buf, rd_buf, len+2) == 0)
            return true;
    }
    
    return false; //Запись не удалась после tries_cnt попыток
}
